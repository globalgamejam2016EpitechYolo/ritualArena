﻿using UnityEngine;
using System.Collections;

public class NetScript : MonoBehaviour {

	public float initialSpeed;
	public float finalSpeed;
	public float accelerationDuration;

	private float timeSinceCreation = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		// Get acceleration
		float interpolant = timeSinceCreation / accelerationDuration;
		if (interpolant > 1f)
			interpolant = 1f; 

		// Ajust time
		if (interpolant < 1f)
			timeSinceCreation += Time.deltaTime;

		// Move object
		Vector3 movement = Vector3.Lerp (Vector3.forward * initialSpeed, Vector3.forward * finalSpeed, interpolant);		
		this.gameObject.transform.Translate (movement * Time.deltaTime);
	}
}
