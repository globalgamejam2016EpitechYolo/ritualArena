﻿
using UnityEngine;
using System.Collections;

public class Player_Controller : MonoBehaviour {
	
	private static int controllerCount = 1;
	private int controllerId;
	private bool isInputActived = true;


	public float speed = 0.1f;

	public string joystick_XInputName = "Horizontal";
	public string joystick_YInputName = "Vertical";

	public float joystick_DeadInput = 0.5f;

	void Start () {
		controllerId = controllerCount++;
	}

	public int getControllerId() {
		return controllerId;
	}

	public void activeInput() {
		isInputActived = true;
	}

	public void desactiveInput() {
		isInputActived = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (!isInputActived)
			return;
		//DEPLACEMENTS
		float Xinput = 0, Yinput = 0;

		// Input joystick
		Yinput = -(Input.GetAxis (joystick_XInputName + controllerId));
		Xinput = (Input.GetAxis (joystick_YInputName + controllerId)); // On inverse l'axe Y

		// Pour le joueur 1, on verifie les input clavier
		// si aucun input joystick n'est repéré
		if (controllerId == 1 && Xinput == 0 && Yinput == 0) {
			// Deplacement clavier
			if (Input.GetButton("MoveUp")) { Xinput = 1; }	
			if (Input.GetButton("MoveDown")) { Xinput = -1; }	
			if (Input.GetButton("MoveLeft")) { Yinput = 1; }	
			if (Input.GetButton("MoveRight")) { Yinput = -1; }	

			if (Xinput != 0 && Yinput != 0) {
				Xinput *= Mathf.Sqrt (2) / 2;
				Yinput *= Mathf.Sqrt (2) / 2;
			}
		} else if (Xinput != 0 || Yinput != 0) {
			//Debug.Log (" CONTROLLER ID : " + controllerId + " xinput : " + Xinput + " / yinput : " + Yinput);
		}

		PlayerScript pl = gameObject.GetComponent<PlayerScript> () as PlayerScript;

		if ((Xinput < -joystick_DeadInput || Xinput > joystick_DeadInput ||
			Yinput < -joystick_DeadInput || Yinput > joystick_DeadInput) &&
			(gameObject.GetComponent<NetOnPlayer> () as NetOnPlayer).isLocked() == false) {

			pl.setMoving(true);
			Vector3 direction = new Vector3 (Xinput , 0f, Yinput);
			this.transform.Translate (direction * speed, Space.World);
			this.transform.forward = direction;
		} else {
			pl.setMoving(false);
		}
			
		// Actions

		if (Input.GetButton ("Pray" + controllerId) ||
			(controllerId == 1 && Input.GetButton ("Pray"))) { // Start incantation
			pl.startIncantation ();
		} else if (((!Input.GetButton ("Pray" + controllerId)) ||
			(controllerId == 1 && !Input.GetButton ("Pray"))) && pl.IsIncanting()) { // Stop incantation
			pl.stopIncantation ();
		}

		if (Input.GetButton ("Mine" + controllerId) ||
			(controllerId == 1 && Input.GetButton ("Mine"))) { // Use mine
			Debug.Log ("Using mine !");
		} else if (Input.GetButton ("Net" + controllerId) ||
			(controllerId == 1 && Input.GetButton ("Net"))) { // Use net
			Debug.Log ("Using net !");
			pl.shootNet ();
		}

	}
}
