﻿using UnityEngine;
using System.Collections;

public class GuiPlayerScript : MonoBehaviour {

	private GameObject mine;
	private GameObject filet;
	private GameObject selector;
	private bool position = true;
	private float currentTime = 0f;

	public int idPlayer = 1;
	private float delay = 0.5f;

	private bool prevWeapon = false;
	private bool nextWeapon = false;

	// Use this for initialization
	void Start () {
		Transform t = GetComponent<Transform>() as Transform;
		mine = t.Find("mine").gameObject;
		filet = t.Find("filet").gameObject;
		selector = t.Find("selector").gameObject;
	}
	
	// Update is called once per frame
	void Update () {
		currentTime += Time.deltaTime;
		if (currentTime >= delay && (nextWeapon || prevWeapon)) {
			position = !position;
			updateSelector();
			currentTime = 0f;
		}
		nextWeapon = false;
		prevWeapon = false;
	}

	private void updateSelector()
	{
		Transform t;
		if (position)
			t = mine.transform;
		else
			t = filet.transform;

		selector.transform.position = t.position;
	}

	// Weapons
	public void useNextWeapon() {
		nextWeapon = true;
	}

	public void usePrevWeapon() {
		prevWeapon = true;
	}
}
