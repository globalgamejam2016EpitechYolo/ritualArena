﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerScript : MonoBehaviour {

	// Team
	private TeamScript team;
	// Map
	private bool alive = true;
	private List<ZoneScript> zones = new List<ZoneScript>();
	// Actions
	private bool isIncanting = false;
	private bool isMoving = false;

	// Object Fight
	private GameObject fightObj;

	// Inventory
	public GameObject netSpawner;



	// Use this for initialization
	void Start () {
		fightObj = (GameObject)GameObject.Instantiate(Resources.Load("FightObject"), Vector3.zero, Quaternion.identity);
	}
	
	// Update is called once per frame
	void Update () {
		if ((gameObject.GetComponent<NetOnPlayer> () as NetOnPlayer).isLocked())
			stopIncantation();
	}

	// Triggers
	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "zone") {
			joinZone (col.gameObject.GetComponent<ZoneScript> () as ZoneScript);
		}
	}

	void OnTriggerExit(Collider col)
	{
		if (col.gameObject.tag == "zone") {	
			leaveZone (col.gameObject.GetComponent<ZoneScript> () as ZoneScript);
		}
	}

	void OnCollisionEnter(Collision col) {
		if (col.gameObject.tag == "team2" && ((TeamScript)team.GetComponent<TeamScript>()).getId() == 1)
		{
			FightScript f = fightObj.GetComponent<FightScript>() as FightScript;
			f.initFight(gameObject, col.gameObject);
			stopIncantation();
		}
	}
	
	/*
	 * Get/Set
	 * */
	public bool IsIncanting()
	{
		return isIncanting;
	}

	/*
	 * Team 
	 */

	public void setTeam(TeamScript team) {
		this.team = team;
	}

	public TeamScript getTeam() {
		return team;
	}

	/*
	 * Map
	 */

	public bool isAlive() {
		return alive;
	}

	public void kill() {
		alive = false;
	}

	public void resurrect() {
		alive = true;
	}

	void joinZone(ZoneScript zone) {
		zones.Add (zone);
	}

	void leaveZone(ZoneScript zone) {
		stopIncantation ();
		zones.Remove (zone);
	}

	/*
	 * Zones
	 */

	bool canIncant() {
		return (zones.Count == 1 && isMoving == false);
	}

	ZoneScript getIncantationTarget() {
		if (zones.Count > 0)
			return zones [0];
		return null;
	}

	public void startIncantation() {
		if (isIncanting)
			return; 
		if (canIncant ()) {
			isIncanting = true;
			getIncantationTarget ().invokeStart(team);
		}
	}

	public void stopIncantation() {
		if (!isIncanting)
			return;
		if (isIncanting) {
			isIncanting = false;
			getIncantationTarget ().invokeStop(team);
		}
	}

	/*
	 * Actions
	 */

	public void setMoving(bool isMoving) {
		if (!this.isMoving && isMoving) {
			stopIncantation ();
		}
		this.isMoving = isMoving;
	}

	public void shootNet() {
		int price = 15;
		if (!team.haveEnoughtPoint(price))
			return;
		team.getPoint(price);
		NetSpawner spawner = netSpawner.GetComponent<NetSpawner> () as NetSpawner;
		spawner.shoot ();
	}
}
