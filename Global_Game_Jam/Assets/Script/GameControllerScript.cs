﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class GameControllerScript : MonoBehaviour {

	public GameObject[] playerPrefab;

	public GameObject teamPrefb;
	

	private List<GameObject> teams = new List<GameObject>();
	private List<GameObject> players = new List<GameObject>();
	private List<GameObject> zones = new List<GameObject>();
	private List<GameObject> zonesDemon = new List<GameObject>();
	private List<GameObject> zonesAnge = new List<GameObject>();
	private List<GameObject> zonesNeutre = new List<GameObject>();

	void Awake() {
		setupZones ();
		setupTeams ();
		setupPlayers ();
	}

	void setupPlayers() {
		int[] spawns = {1, 6, 7, 13};
		foreach (int index in Enumerable.Range(0, 4)) {
			// Get spawn zone
			int spawnId = spawns[index]; 
			GameObject zoneObj = zones.Find (zone => (zone.GetComponent<ZoneScript> () as ZoneScript).zoneIdentifier == spawnId);
			GameObject spawner = (zoneObj.GetComponent<ZoneScript> () as ZoneScript).playerSpawner;
			Vector3 spawn = spawner.transform.position;
			// Create player
			GameObject playerObj = Instantiate (playerPrefab[index], new Vector3 (spawn.x, 0.7f, spawn.z), Quaternion.identity) as GameObject;
			playerObj.transform.tag = "team" + ((index >= 2) ? 2 : 1);
			PlayerScript player = playerObj.GetComponent<PlayerScript>() as PlayerScript;
			player.setTeam ((index < 2 ? teams [0] : teams [1]).GetComponent<TeamScript>() as TeamScript);
			players.Add (playerObj);
		}
	}

	void setupTeams() {
		foreach (int index in Enumerable.Range(0, 2)) {
			GameObject teamObj = Instantiate(teamPrefb, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
			TeamScript team = teamObj.GetComponent<TeamScript>() as TeamScript;
			teamObj.name = (index == 0 ? "teamOne" : "teamTwo");
			team.setId (index + 1);
			team.init();
			teams.Add (teamObj);
		}
	}

	void setupZones() {
		string[] themes = {"demonZone", "angeZone", "neutreZone"};
		foreach (string theme in themes) {
			for (int i = 1; i < 18; i++) {
				GameObject zone = GameObject.Find(theme + i);
				if (theme.Equals("demonZone"))
					zonesDemon.Add(zone);
				else if (theme.Equals("angeZone"))
					zonesAnge.Add(zone);
				else
					zonesNeutre.Add(zone);
				if (theme.Equals("demonZone") || theme.Equals("angeZone"))
					zone.SetActive(false);
			}
		}

		foreach (GameObject zone in GameObject.FindGameObjectsWithTag ("zone")) {
			zones.Add (zone);
			ZoneScript zs = zone.GetComponent<ZoneScript>() as ZoneScript;
			zs.setZones(zonesDemon[zs.zoneIdentifier - 1], zonesAnge[zs.zoneIdentifier - 1], zonesNeutre[zs.zoneIdentifier - 1]);
		}
	}

	void Update() {
		// Respawn players
		foreach (GameObject playerObj in players) {
			PlayerScript player = playerObj.GetComponent<PlayerScript> () as PlayerScript;
			if (!player.isAlive ()) {
				List<GameObject> availableZones = zones.Where (zone => ((zone.GetComponent<ZoneScript> () as ZoneScript).idTeamZone == player.getTeam ().getId ())) as List<GameObject>;
				if (availableZones != null && availableZones.Count > 0) { // Si une zone est controllée par l'equipe du joueur, respawn dans cette zone
					int spawnIndex = Random.Range (0, availableZones.Count);
					player.transform.position = new Vector3 (availableZones [spawnIndex].transform.position.x, playerObj.transform.position.y, availableZones [spawnIndex].transform.position.z);
				} else { // Sinon, respawn au centre de la carte
					player.transform.position = new Vector3 (0f, playerObj.transform.position.y, 0f);
				}
				player.resurrect ();
			}
		}

		// Score limit
		foreach (GameObject teamObj in teams) {
			TeamScript team = teamObj.GetComponent<TeamScript> () as TeamScript;
			if (team.haveEnoughtPoint (5000)) {
				Debug.Log ("TEAM WINS");
				//Application.LoadScene ("Menu_LD");
			}
		}

	}
}
