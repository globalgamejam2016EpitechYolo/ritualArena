﻿using UnityEngine;
using System.Collections;

public class ZoneScript : MonoBehaviour {

	// Identifier and content
	public int zoneIdentifier;
	public GameObject playerSpawner;

	// barre to show status of capture
	private GameObject blankBarre;
	private GameObject neutralBarre;
	private GameObject teamOneBarre;
	private GameObject teamTwoBarre;

	// Teams
	private GameObject teamOne;
	private GameObject teamTwo;

	// Theme Zones
	private GameObject demon;
	private GameObject ange;
	private GameObject neutre;

	public int pointPerSecond = 2;

	public int rateCapture = 2;
	private int minNeutralZone = -30;
	private int maxNeutralZone = 30;

	private int nbTeamOne = 0;
	private int nbTeamTwo = 0;

	private int maxZone = 100;
	private int minZone = -100;

	// -1 pas de team, 1 teamOne, 2 teamTwo
	public int idTeamInvoking = -1;
	public int idTeamZone = -1;

	private float currentTime = 0f;

	private enum STATE {
		NEUTRAL,
		CAPTURED
	};
	
	// Between -100 and 100
	private int positionCapture = 0;
	private bool isInvoked = false;
	private STATE _state = STATE.NEUTRAL;

	// Use this for initialization
	void Start () {
		teamOne = GameObject.Find("teamOne");
		teamTwo = GameObject.Find("teamTwo");

		// Instantiate all the prefab for bar
		blankBarre = (GameObject)GameObject.Instantiate(Resources.Load("blank", typeof(GameObject)), Vector3.zero, Quaternion.identity);
		blankBarre.SetActive(false);
		neutralBarre = (GameObject)GameObject.Instantiate(Resources.Load("neutralbarre", typeof(GameObject)), Vector3.zero, Quaternion.identity);
		neutralBarre.SetActive(false);
		teamOneBarre = (GameObject)GameObject.Instantiate(Resources.Load("teamOneBarre", typeof(GameObject)), Vector3.zero, Quaternion.identity);
		teamOneBarre.SetActive(false);
		teamTwoBarre = (GameObject)GameObject.Instantiate(Resources.Load("teamTwoBarre", typeof(GameObject)), Vector3.zero, Quaternion.identity);
		teamTwoBarre.SetActive(false);

		InvokeRepeating("GainPointTeam", 0f, 1.0f);
	}

	//SET ZONES
	public void setZones(GameObject d, GameObject a, GameObject n) {
		demon = d;
		ange = a;
		neutre = n;
	}

	// Update is called once per frame
	void Update () {
		if (isInvoked) {
			currentTime += Time.deltaTime;
			if (currentTime >= 0.1f) {
				GainPointControl();
				currentTime -= 0.1f;
				showBarre();
			}


			/* NEUTRAL > CAPTURED || CAPTURED > NEUTRAL */
			if ((positionCapture >= maxNeutralZone
				    || positionCapture <= minNeutralZone)
				    && _state == STATE.NEUTRAL)
				ZoneCaptured();
			else if (positionCapture > minNeutralZone 
				         && positionCapture < maxNeutralZone
				         && _state == STATE.CAPTURED)
				ZoneNeutralized();
			/* ****** */

		}
	}

	private void GainPointControl() {
		if (idTeamInvoking == 1)
			positionCapture += rateCapture;
		else if (idTeamInvoking == 2)
			positionCapture -= rateCapture;

		/*if (idTeamInvoking > 0)
			Debug.Log("Gain POINT control (" + positionCapture+")");*/

		isInvoked = (idTeamInvoking == -1) ? false : true;


		if (positionCapture < minZone)
			positionCapture = minZone;
		else if (positionCapture > maxZone)
			positionCapture = maxZone;
	}

	private void ZoneCaptured() {
		// La zone est capturée par l'équipe idTeam
		//Debug.Log("Zone capturée par l'équipe " + idTeamInvoking);
		idTeamZone = idTeamInvoking;
		_state = STATE.CAPTURED;

		neutre.SetActive(false);
		if (idTeamZone == 1) {
			ange.SetActive(true);
			demon.SetActive(false);
		}
		else {
			ange.SetActive(false);
			demon.SetActive(true);
		}
	}

	private void ZoneNeutralized() {
		// la zone est redevenu neutre. 
	//	Debug.Log("Zone neutralisée par l'équipe " + idTeamInvoking);
		idTeamZone = -1;
		_state = STATE.NEUTRAL;

		ange.SetActive(false);
		demon.SetActive(false);
		neutre.SetActive(true);
	}

	private void GainPointTeam() {
		if (_state == STATE.CAPTURED) {
			TeamScript t;

			if (idTeamZone == 1)
				t = teamOne.GetComponent<TeamScript>() as TeamScript;
			else if (idTeamZone == 2)
				t = teamTwo.GetComponent<TeamScript>() as TeamScript;
			else
				return;
			if (t)
				t.addPoint(pointPerSecond);
			else
				Debug.Log("Can't retrieve TeamScript");
		}
	}

	public void invokeStart(TeamScript idTeam)
	{
		//Debug.Log("Start INVOC : point de capture : " + positionCapture);
		if (isInvoked && idTeam.getId() == idTeamInvoking)
			return;
		Debug.Log ("Test : " + idTeam.getId() + " State : nbTeamone : " + nbTeamOne + " nbTeamTwo : "  + nbTeamTwo);
		if (idTeam.getId() == 1)
			nbTeamOne++;
		else if (idTeam.getId() == 2)
			nbTeamTwo++;
		setInvokatingTeam();
		if (idTeamInvoking > 0) {
			isInvoked = true;
			showBarre();
		}
		else {
			isInvoked = false;
			hideBarre();
		}
	}

	public void invokeStop(TeamScript idTeam)
	{
		//Debug.Log("Stop INVOC");
		if (!isInvoked && 
		    ((nbTeamOne == 0 && idTeam.getId () == 1) || (nbTeamTwo == 0 && idTeam.getId() == 2)))
			return;
		isInvoked = false;
		if (idTeam.getId() == 1)
			nbTeamOne--;
		else if (idTeam.getId() == 2)
			nbTeamTwo--;
		setInvokatingTeam();
		if (idTeamInvoking > 0) {
			isInvoked = true;
			showBarre();
		}
		else {
			isInvoked = false;
			hideBarre();
		}
	}

	private void setInvokatingTeam()
	{
		if (nbTeamOne < 0)
			nbTeamOne = 0;
		if (nbTeamTwo < 0)
			nbTeamTwo = 0;
		if (nbTeamOne > 2)
			nbTeamOne = 2;
		if (nbTeamTwo > 2)
			nbTeamTwo = 2;

		if (nbTeamOne > nbTeamTwo)
			idTeamInvoking = 1;
		else if (nbTeamOne < nbTeamTwo)
			idTeamInvoking = 2;
		else
			idTeamInvoking = -1;
		Debug.Log ("iD TEAM : " + idTeamInvoking);
	}

	private void hideBarre()
	{
		blankBarre.SetActive(false);
		neutralBarre.SetActive(false);
		teamOneBarre.SetActive(false);
		teamTwoBarre.SetActive(false);
	}
	
	private void showBarre() {
		Transform tblank = blankBarre.GetComponent<Transform>() as Transform;
		Transform tNeutre = neutralBarre.GetComponent<Transform>() as Transform;
		Transform tTwo = teamTwoBarre.GetComponent<Transform>() as Transform;
		Transform tOne = teamOneBarre.GetComponent<Transform>() as Transform;
		
		//Apply rotate
		tblank.localEulerAngles  =  new Vector3(0f,0f,17f);
		tNeutre.localEulerAngles  =  new Vector3(0f,0f,17f);
		tTwo.localEulerAngles  =  new Vector3(0f,0f,17f);
		tOne.localEulerAngles  =  new Vector3(0f,0f,17f);
		
		
		// APPLY scale 
		
		if (idTeamInvoking == 1) {
			if (positionCapture < 0f){
				tTwo.localScale = new Vector3(tTwo.localScale.x, 0f, (-1f * (float)positionCapture / (float)maxZone) * 0.5f);
				teamTwoBarre.SetActive(true);
			}
			else
				teamTwoBarre.SetActive(false);
			if (positionCapture >= 0) {
				tOne.localScale = new Vector3(tOne.localScale.x, 0f, ((float)positionCapture / (float)maxZone) * 0.5f);
				teamOneBarre.SetActive(true);
			}
			else
				teamOneBarre.SetActive(false);
		}
		else if (idTeamInvoking == 2) {
			if (positionCapture <= 0){
				tTwo.localScale = new Vector3(tTwo.localScale.x, 0f, (-1f * (float)positionCapture / (float)maxZone) * 0.5f);
				teamTwoBarre.SetActive(true);
			}
			else
				teamTwoBarre.SetActive(false);
			if (positionCapture > 0f) {
				tOne.localScale = new Vector3(tOne.localScale.x, 0f, ((float)positionCapture / (float)maxZone) * 0.5f);
				teamOneBarre.SetActive(true);
			}
			else
				teamOneBarre.SetActive(false);
		}
		tblank.localScale = new Vector3(0.1f, 0f, 0.5f);
		tNeutre.localScale = new Vector3(0.1f, 0f, 0.005f);
		// Show item to be shown
		blankBarre.SetActive(true);
		neutralBarre.SetActive(true);

		GameObject Totem = findTotem();


		// Apply operation on z to align plan
		if (Totem == null) {  // VA DISPARAITRE LAISSE CAR PAS DE TOTEM EN NEUTRE
			float x = ((Collider)neutralBarre.GetComponent<Collider>()).bounds.size.z * 0.5f + ((Collider)blankBarre.GetComponent<Collider>()).bounds.size.z * 0.1f;
			tNeutre.position = new Vector3(transform.position.x , 10.01f, transform.position.z - x);
			
			x = ((Collider)blankBarre.GetComponent<Collider>()).bounds.size.z * 0.5f;
			tblank.position = new Vector3(transform.position.x, 9.99f, transform.position.z - x);
			
			x = ((Collider)teamTwoBarre.GetComponent<Collider>()).bounds.size.z * 0.5f;
			tTwo.position = new Vector3(transform.position.x, 10f, transform.position.z - x);
			
			x = ((Collider)teamOneBarre.GetComponent<Collider>()).bounds.size.z * 0.5f;
			tOne.position = new Vector3(transform.position.x, 10f, transform.position.z - x);
		}
		else {
			Transform t = Totem.GetComponent<Transform>() as Transform;
			float x = ((Collider)neutralBarre.GetComponent<Collider>()).bounds.size.z * 0.5f +((Collider)blankBarre.GetComponent<Collider>()).bounds.size.z * ((float)maxNeutralZone / 100f);
			tNeutre.position = new Vector3(t.position.x , 10.01f, t.position.z - x);
			
			x = ((Collider)blankBarre.GetComponent<Collider>()).bounds.size.z * 0.5f;
			tblank.position = new Vector3(t.position.x, 9.99f, t.position.z - x);

			x = ((Collider)teamTwoBarre.GetComponent<Collider>()).bounds.size.z * 0.5f;
			tTwo.position = new Vector3(t.position.x, 10f, t.position.z - x);
			
			x = ((Collider)teamOneBarre.GetComponent<Collider>()).bounds.size.z * 0.5f;
			tOne.position = new Vector3(t.position.x, 10f, t.position.z - x);
		}
	}

	private GameObject findTotem()
	{
		GameObject refToSearch;
		string name;

		if (idTeamZone == 1) {
			refToSearch = ange;
			name = "Totem_Ange";
		}
		else if (idTeamZone == 2) {
			refToSearch = demon;
			name = "Totem_Demon";
		}
		else {
			refToSearch = neutre;
			name = "Totem_Neutre";
			// Pas encore implémenté
		}
		GameObject totem = null;
 // CONDITION TEMPORAIRE CAR PAS DE TOTEM EN NEUTRE
			Transform t = refToSearch.GetComponent<Transform>() as Transform;
			totem = t.Find(name).gameObject as GameObject;
		return totem;
	}

}
