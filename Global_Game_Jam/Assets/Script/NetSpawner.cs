﻿using UnityEngine;
using System.Collections;

public class NetSpawner : MonoBehaviour {

	public GameObject netPrefab;
	public float shootDelay;

	private float timeSinceLastShot = 0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		timeSinceLastShot += Time.deltaTime;
	}

	public void shoot() {
		if (timeSinceLastShot < shootDelay)
			return;
		timeSinceLastShot = 0f;
		GameObject net = Instantiate (netPrefab, this.gameObject.transform.position, Quaternion.identity) as GameObject;
		net.transform.forward = this.gameObject.transform.forward;
	}
}
