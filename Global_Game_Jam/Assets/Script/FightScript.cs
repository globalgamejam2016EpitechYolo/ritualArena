﻿using UnityEngine;
using System.Collections;

public class FightScript : MonoBehaviour {

	private GameObject playerOne;
	private GameObject playerTwo;

	private GameObject gui;
	private bool isInit = false;

	private float totalTime = 3f;
	private float currentTime = 0f;

	private float totalTimeAnim = 0.1f;
	private float currentTimeAnim = 0f;

	private int countOne = 0;
	private int countTwo = 0;

	private float state = 0.2f;

	private int winningPoint = 50;

	private bool CanX1 = true;
	private bool CanX2 = true;

	void Start () {
		gui = transform.Find("display").gameObject;
		gui.SetActive(false);
	}

	public void initFight(GameObject playerOne, GameObject playerTwo)
	{
		this.playerOne = playerOne;
		this.playerTwo = playerTwo;

		toggleInput(false);

		gui.transform.position = new Vector3(playerOne.transform.position.x, gui.transform.position.y, playerOne.transform.position.z);
		currentTime = 0f;
		isInit = true;
		countOne = 0;
		countTwo = 0;
		gui.SetActive(true);
	}

	// Update is called once per frame
	void Update () {
		if (!isInit)
			return;

		Player_Controller pl1 = playerOne.GetComponent<Player_Controller>() as Player_Controller;
		Player_Controller pl2 = playerTwo.GetComponent<Player_Controller>() as Player_Controller;

		if (Input.GetKeyDown ("joystick "+pl1.getControllerId()+" button 2"))
		{
			countOne++;
		}
		if (Input.GetKeyDown ("joystick "+pl2.getControllerId()+" button 2"))
		{
			countTwo++;
		}
		Debug.Log ("1 ("+countOne+") vs 2("+countTwo+")");
		currentTimeAnim += Time.deltaTime;
		gui.transform.localScale = Vector3.Lerp(gui.transform.localScale, new Vector3(state, state, state), currentTimeAnim / totalTimeAnim);
		if (currentTimeAnim >= totalTimeAnim) {
			state = (state == 0.2f) ? 0.15f : 0.2f;
			currentTimeAnim = 0f;
		}

		currentTime += Time.deltaTime;
		if (currentTime >= totalTime) {
			EndFight();
		}
	}

	void EndFight() {

		PlayerScript winner;
		PlayerScript looser;
		TeamScript teamWinner;

		if (countOne > countTwo) {
			winner = playerOne.GetComponent<PlayerScript>() as PlayerScript;
			looser = playerTwo.GetComponent<PlayerScript>() as PlayerScript;
			teamWinner = GameObject.Find("teamOne").GetComponent<TeamScript>() as TeamScript;
		} else {
			looser = playerOne.GetComponent<PlayerScript>() as PlayerScript;
			winner = playerTwo.GetComponent<PlayerScript>() as PlayerScript;
			teamWinner = GameObject.Find("teamTwo").GetComponent<TeamScript>() as TeamScript;
		}

		looser.kill();
		teamWinner.addPoint(winningPoint);

		toggleInput(true);
		gui.SetActive(false);

		// Destroy Fight Object
		//Destroy(gameObject, 1f);
		isInit = false;
	}

	void toggleInput(bool op)
	{
		Player_Controller one = playerOne.GetComponent<Player_Controller>() as Player_Controller;
		Player_Controller two = playerTwo.GetComponent<Player_Controller>() as Player_Controller;

		if (op) {
			one.activeInput();
			two.activeInput();
		} else {
			one.desactiveInput();
			two.desactiveInput();
		}
	}
}
