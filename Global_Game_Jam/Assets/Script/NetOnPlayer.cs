﻿using UnityEngine;
using System.Collections;

public class NetOnPlayer : MonoBehaviour {

	public float lockDuration;

	private float lockedFor = 0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (!isLocked())
			return;
		lockedFor -= Time.deltaTime;
	}

	void OnCollisionEnter(Collision col) {
		if (!(col.gameObject.tag == "net"))
			return;
		lockedFor = lockDuration;
	}

	public bool isLocked() {
		return lockedFor > 0f;
	}
}
