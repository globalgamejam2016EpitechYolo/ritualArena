﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TeamScript : MonoBehaviour {
	
	int point = 0;
	int id;

	private Text Score;

	public void init()
	{
		Score = ((GameObject)GameObject.Find("TeamText" + id)).GetComponent<Text>() as Text;
		setText ();
	}

	public void setId(int _id)
	{
		id = _id;
	}

	public int getId()
	{
		return id;
	}

	public void addPoint(int add)
	{
		point += add;
		setText();
	}

	public bool haveEnoughtPoint(int toGet)
	{
		if (point >= toGet)
			return true;
		return false;
	}

	public int getPoint(int toGet)
	{
		if (!haveEnoughtPoint(toGet))
			return 0;
		point -= toGet;
		return toGet;
	}

	private void setText()
	{
		string tmp = (id == 1) ? "Team Ange " : "Team Demon ";
		Score.text = tmp + ": " + point;
	}
}
